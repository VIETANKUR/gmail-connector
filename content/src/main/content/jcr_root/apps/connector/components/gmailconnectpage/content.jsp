<%@page session="false" contentType="text/html" pageEncoding="utf-8"
	import="com.accunity.connector.services.ContentService, com.day.cq.i18n.I18n,
                org.apache.sling.api.resource.ValueMap,
                org.apache.sling.api.resource.Resource,
                com.adobe.granite.auth.oauth.ProviderConfigProperties,
                javax.jcr.Session, java.util.Map"%>

<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/libs/social/security/social-security.jsp"%>

<%
    I18n i18n = new I18n(slingRequest);
    final Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
    final boolean editable = session.hasPermission(resource.getPath(),
            Session.ACTION_SET_PROPERTY);
%>


<div class="connect-content">
	<h3>Gmail Connect Configuration Settings</h3>

	<img
		src="<%=xssAttrWrap(xssAPI, currentNode.getProperty("thumbnailPath").getString())%>"
		alt="Gmail Connect" title="Gmail Connect" class="fbconnect-thumbnail" />

	<%
	    if (editable) {
	%>
	<ul class="fbconnect-content">
		<%
		    ContentService contentSvc = sling.getService(ContentService.class);
		        String errMessage = contentSvc.validateConfigurations(currentPage, resource,
		                ProviderConfigProperties.FACTORY_PID, "oauth.config.id");
		        if (errMessage != null) {
		%>
		<li><div class="li-bullet">
				<%=i18n.get(errMessage)%></li>

		<%
		    } else {
		            Resource configResource = contentSvc.getConfigurationResource(
		                    currentPage.getName(), resource, ProviderConfigProperties.FACTORY_PID);
		            ValueMap props = configResource.adaptTo(ValueMap.class);
		%>
		<c:set var="clientId"
			value="<%=xssFilterWrap(xssAPI, props.get("oauth.client.id"))%>" />
		<%
		    Map<String, String> clientConfMap = contentSvc.getClientConfigurations(props);
		            for (Map.Entry<String, String> confEntry : clientConfMap.entrySet()) {
		%>
		<li>
			<div class="li-bullet">
				<strong><%=i18n.get(confEntry.getKey())%></strong>
				<%=xssFilterWrap(xssAPI, confEntry.getValue())%>
			</div>
		</li>
		<%
		    }

		            Map<String, String> permissionsMap = contentSvc.getConfigurations(props);
		            for (Map.Entry<String, String> permEntry : permissionsMap.entrySet()) {
		%>
		<li>

			<div class="li-bullet">
				<div class="fbconnect-div-comma-sep-list">
					<strong><%=i18n.get(permEntry.getKey())%></strong>
					<%=xssFilterWrap(xssAPI, permEntry.getValue())%>
				</div>
			</div>
		</li>

		<%
		    }
		%>

		<li class="when-config-successful" style="display: none;">Gmail
			Connect Configuration is successful.</li>
		<%
		    }
		%>

		<li><div class="li-bullet">
				<button onclick="dialog.show()"><%=i18n.get("Edit Configuration")%></button>
			</div></li>
	</ul>
	<%
	    }
	%>
</div>
