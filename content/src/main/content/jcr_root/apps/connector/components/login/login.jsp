<%@include file="/libs/foundation/global.jsp" %>
<%@page contentType="text/html; charset=utf-8" import="com.accunity.connector.services.LoginService,com.accunity.connector.common.ConnectorConstants" %>

<cq:includeClientLib categories="connector.lib, cq.jquery"/>

<%
	LoginService loginSvc = sling.getService(LoginService.class);
	String gmailClientId = loginSvc.login(pageProperties, ConnectorConstants.GMAIL_CONNECT);
%>

<meta name="google-signin-client_id" content="<%= gmailClientId %>">
<input type="hidden" id="resPath" value="<%=pageProperties.get(ConnectorConstants.CQ_CLOUD_SVC_CFG)%>" />
<div class="g-signin2" data-onsuccess="onSignIn"></div>
<a href="#" onclick="signOut();">Sign out</a>