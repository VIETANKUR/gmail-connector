<%@page session="false" contentType="text/html"
            pageEncoding="utf-8"
            import="javax.jcr.Node,
                    java.util.Iterator,
                    com.day.cq.wcm.webservicesupport.Configuration,
                    com.day.cq.wcm.webservicesupport.Service,
                    org.apache.commons.lang.StringEscapeUtils,
                    org.apache.sling.api.resource.Resource"%>


<%@include file="/libs/foundation/global.jsp"%>
<%@include file="/libs/cq/cloudserviceconfigs/components/configpage/init.jsp"%><%
%><%@include file="/libs/social/security/social-security.jsp" %>

<cq:includeClientLib categories="cq.social.connect"/>

<%
    final String id = currentPage.getName();
    final String title = xssAPI.encodeForHTML(properties.get("jcr:title", id));
    final String description = xssAPI.encodeForHTML(properties.get("jcr:description", ""));
    final String path = resource.getPath();
    final String resourceType = resource.getResourceType();
	final String dialogPath = (resourceType.startsWith("/libs") ? "/libs/" : "") + resourceType + "/dialog";

%>

<body>

    <script type="text/javascript">
        var dialog = CQ.WCM.getDialog("<%=xssJSWrap(xssAPI, dialogPath)%>");
        console.log(dialog);
        dialog.loadContent(CQ.WCM.getPagePath() + "/jcr:content.configvalues.json");
        dialog.on('beforesubmit', $CQ.SocialAuth.osgi.updateOAuthProviderConfig);
    </script>

    <h1><%=xssPass(xssAPI, title)%></h1>

    <p><%=xssPass(xssAPI, description)%></p>

    <cq:include script="content.jsp" />
    <cq:include script="opendialog.jsp" />
</body>
