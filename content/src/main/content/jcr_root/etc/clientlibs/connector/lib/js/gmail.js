function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    var path = document.getElementById("resPath").value;
    //Use JQuery AJAX request to post data to a Sling Servlet
    $.ajax({
         type: 'POST',    
         url:'/bin/user',
         data:'email='+ profile.getEmail() +'&name='+ profile.getName() + '&path='+path,
         success: function(msg){
           console.log("success");
         }
     });
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}
