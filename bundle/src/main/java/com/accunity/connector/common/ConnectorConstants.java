package com.accunity.connector.common;

/**
 * Constant class storing all the required constants for the login connector.
 * 
 * @author Accunity Software
 * 
 */
public class ConnectorConstants {

    /**
     * Constant to store the empty string
     */
    public static String EMPTY_STRING = "";
    
    /**
     * Constant to store the space string
     */
    public static String SPACE_STRING = " ";
    
    /**
     * Constant holding the string '<b>, </b>'
     */
    public static String COMMA_SPACE = ", ";

    /**
     * Constant holding the string <b>cq:cloudserviceconfigs</b>
     */
    public static String CQ_CLOUD_SVC_CFG = "cq:cloudserviceconfigs";

    /**
     * Constant holding the string <b>gmailconnect</b>
     */
    public static String GMAIL_CONNECT = "gmailconnect";

    /**
     * Constant holding the string <b>jcr:content</b>
     */
    public static String JCR_CONTENT = "jcr:content";

    /**
     * Constant holding the string <b>oauth.client.id</b>
     */
    public static String OAUTH_CLIENT_ID = "oauth.client.id";

    /**
     * Constant holding the string <b>oauth.create.users</b>
     */
    public static String OAUTH_CREATE_USER = "oauth.create.users";

    /**
     * Constant holding the suffix <b>.config</b>
     */
    public static String DOT_CONFIG_SUFFIX = ".config";

    /**
     * Constant holding the string <b>-</b>
     */
    public static String HYPHEN = "-";

    /**
     * Constant holding the string <b>friend</b>
     */
    public static String FRIEND_STRING = "friend";

    /**
     * Constant holding the string <b>user</b>
     */
    public static String USER_STRING = "user";

    /**
     * Constant holding the string <b>_</b>
     */
    public static String UNDERSCORE = "_";
    
    /**
     * Constant holding the string <b>urlParams</b>
     */
    public static String URL_PARAMS = "urlParams";
    
    /**
     * Constant holding the string <b>oauth.scope</b>
     */
    public static String OAUTH_DOT_SCOPE = "oauth.scope";
    
    /**
     * Constant holding the string <b>oauth.client.secret</b>
     */
    public static String OAUTH_CLIENT_SECRET = "oauth.client.secret";
    
    /**
     * Constant holding the string <b>oauth.create.users.groups</b>
     */
    public static final String OAUTH_CREATE_USER_GROUPS = "oauth.create.users.groups";
    
    /**
     * Constant holding the string <b>oauth.create.users.groups</b>
     */
    public static final String OAUTH_ENCODE_USER_ID = "oauth.encode.userids";
    
    /**
     * Constant holding the string <b>Mask User Ids:</b>
     */
    public static final String MASK_USER_ID = "Mask User Ids:";
    
    /**
     * Constant holding the string <b>App ID/API Key:</b>
     */
    public static final String APP_ID = "App ID/API Key:";
    
    /**
     * Constant holding the string <b>App Secret:</b>
     */
    public static final String APP_SECRET = "App Secret:";
    
    /**
     * Constant holding the string <b>Add to User Groups:</b>
     */
    public static final String ADD_TO_USER_GROUPS = "Add to User Groups:";

    /**
     * Constant holding the string <b>Create Users:</b>
     */
    public static final String CREATE_USERS = "Create Users:";
    
    /**
     * Constant holding the string <b>User Permissions:</b>
     */
    public static String USER_PERMISSIONS = "User Permissions:";
    
    /**
     * Constant holding the string <b>User Permissions:</b>
     */
    public static String FRIEND_PERMISSIONS = "Friend Permissions:";
    
    /**
     * Constant holding the string <b>User Permissions:</b>
     */
    public static String EXTNDED_PERMISSIONS = "Extended Permissions:";
    
    /**
     * Constant holding the string <b>URL Parameters:</b>
     */
    public static String URL_PARAMETERS = "URL Parameters:";
    
    /**
     * Constant holding the error message in case of invalid configuration set up
     */
    public static String ERR_INVALID_CONFIGS = "No configuration for this cloud service has been found...setup is not complete.<br/>Edit this Configuration to complete the setup process.";
}
