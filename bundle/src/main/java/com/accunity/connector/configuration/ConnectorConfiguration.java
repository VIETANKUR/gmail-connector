package com.accunity.connector.configuration;

import org.apache.felix.scr.annotations.Component;
import org.apache.sling.api.resource.Resource;

import com.accunity.connector.common.ConnectorConstants;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.webservicesupport.Configuration;
import com.day.cq.wcm.webservicesupport.ConfigurationManager;

/**
 * This utility class is used to retrieve the configurations as per the selected connectors.
 * 
 * @author Accunity Software
 * 
 */
@Component
public class ConnectorConfiguration {

    /**
     * This method is used to filter out the connector services from the input services.
     * 
     * @param connectorSvc
     *            Connector Configurations to be filter as per this input
     * @param pageProperties
     *            {@link InheritanceValueMap}
     * @return {@link Configuration}
     */
    public Configuration getConfiguration(String connectorSvc, InheritanceValueMap pageProperties,
            ConfigurationManager cfgMgr) {
        Configuration configuration = null;
        // Retrieve services for the input social connector.
        final String[] services = pageProperties.getInherited(ConnectorConstants.CQ_CLOUD_SVC_CFG,
                new String[] {});
        if (cfgMgr != null) {
            configuration = cfgMgr.getConfiguration(connectorSvc, services);
        }
        return configuration;
    }

    /**
     * This method is used to validate the configurations of the login connector.
     * 
     * @param currentPage
     *            reference to current page to obtain the properties. {@link Page}
     * @param resource
     *            reference to the resource object to obtain the child of the configurations.
     *            {@link Resource}
     * @param factoryPid
     *            Factory Process Id
     * @param configPropName
     *            property name whose value is fetched using current page reference
     * @return true/false; true: if valid configurations, false: otherwise
     */
    public boolean validateConfigurations(Page currentPage, Resource resource, String factoryPid,
            String configPropName) {
        boolean isValid = Boolean.TRUE;
        String configId = currentPage.getProperties().get(configPropName, String.class);

        if (configId != null && configId.length() > 0) {

            String configResourceName = factoryPid + ConnectorConstants.HYPHEN
                    + currentPage.getName() + ConnectorConstants.DOT_CONFIG_SUFFIX;
            Resource configResource = resource.getChild(configResourceName);
            if (configResource == null) {
                isValid = Boolean.FALSE;
            }
        } else {
            isValid = Boolean.FALSE;
        }

        return isValid;

    }

    /**
     * This method is used to retrieve the resource associated with the configuration.
     * 
     * @param pageName
     *            name of the current page
     * @param resource
     *            resource to fetch the child resources
     * @param factoryPid
     *            Factory Process Id
     * @return {@link Resource}
     */
    public Resource getConfigurationResource(String pageName, Resource resource, String factoryPid) {
        return resource.getChild(factoryPid + ConnectorConstants.HYPHEN + pageName
                + ConnectorConstants.DOT_CONFIG_SUFFIX);

    }

}
