package com.accunity.connector.services;

import java.util.Iterator;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.accunity.connector.common.ConnectorConstants;
import com.accunity.connector.configuration.ConnectorConfiguration;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.webservicesupport.Configuration;
import com.day.cq.wcm.webservicesupport.ConfigurationManager;

@Component(enabled = true, immediate = true)
@Service
public class LoginServiceImpl implements LoginService {
    
    @Reference
    ConfigurationManager cfgMgr;

    /**
     * {@inheritDoc}
     */
    public String login(InheritanceValueMap pageProperties, String connectorType) {
        String clientId = ConnectorConstants.EMPTY_STRING;

        Configuration connectorCfg = new ConnectorConfiguration().getConfiguration(connectorType,
                pageProperties, cfgMgr);

        // Iterate through the child nodes of the connector.
        if (connectorCfg != null) {
            Resource parentResource = connectorCfg.getResource().getChild(
                    ConnectorConstants.JCR_CONTENT);
            Iterator<Resource> childItr = null;
            if (parentResource != null && parentResource.hasChildren()) {
                childItr = parentResource.listChildren();
            }
            while (childItr.hasNext()) {
                Resource childRes = childItr.next();
                ValueMap resProperties = childRes.getValueMap();
                if (resProperties.containsKey(ConnectorConstants.OAUTH_CLIENT_ID)) {
                    clientId = resProperties.get(ConnectorConstants.OAUTH_CLIENT_ID, String.class);
                }
            }
        }
        return clientId;
    }

}
