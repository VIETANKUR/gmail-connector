package com.accunity.connector.services;

import com.day.cq.commons.inherit.InheritanceValueMap;

/**
 * This interface is used by {@link LoginServiceImpl} to implement the login functionality via login
 * connector
 * 
 * @author Accunity Software
 * 
 */
public interface LoginService {

    /**
     * This implementation will provide the client id.
     * 
     * @param pageProperties
     *            {@link InheritanceValueMap}
     * @param connectorType
     *            connector type: gmail, facebook, twitter
     * @return client id
     */
    String login(InheritanceValueMap pageProperties, String connectorType);

}
