package com.accunity.connector.services;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.accunity.connector.common.ConnectorConstants;
import com.accunity.connector.configuration.ConnectorConfiguration;
import com.day.cq.wcm.api.Page;

/**
 * Implementation of {@link ContentService}
 * 
 * @author Accunity Software
 * 
 */
@Component(enabled = true, immediate = true)
@Service
public class ContentServiceImpl implements ContentService {

    /**
     * {@inheritDoc}
     */
    public String validateConfigurations(Page currentPage, Resource resource, String factoryPid,
            String configPropName) {
        String errMessage = null;
        boolean isValid = new ConnectorConfiguration().validateConfigurations(currentPage,
                resource, factoryPid, configPropName);

        if (!isValid) {
            errMessage = ConnectorConstants.ERR_INVALID_CONFIGS;
        }
        return errMessage;
    }

    /**
     * {@inheritDoc}
     */
    public Resource getConfigurationResource(String pageName, Resource resource, String factoryPid) {
        return new ConnectorConfiguration()
                .getConfigurationResource(pageName, resource, factoryPid);
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, String> getConfigurations(ValueMap properties) {
        Map<String, String> configMap = new LinkedHashMap<String, String>();

        String userGroups = convertArrayToString(properties,
                ConnectorConstants.OAUTH_CREATE_USER_GROUPS);
        addToConfigMap(configMap, ConnectorConstants.ADD_TO_USER_GROUPS, userGroups);
        getPermissions(configMap, properties, ConnectorConstants.OAUTH_DOT_SCOPE);
        String urlParams = convertArrayToString(properties, ConnectorConstants.URL_PARAMS);
        addToConfigMap(configMap, ConnectorConstants.URL_PARAMETERS, urlParams);

        return configMap;
    }

    /**
     * This method is used to fetch the permissions for the given connector.
     * 
     * @param configMap
     *            Configuration Map
     * @param props
     *            Map of properties
     * @param propName
     *            Property Name
     */
    private void getPermissions(Map<String, String> configMap, ValueMap props, String propName) {
        StringBuilder userSb = new StringBuilder();
        StringBuilder friendSb = new StringBuilder();
        StringBuilder extSb = new StringBuilder();

        String[] scopesSplit = (String[]) props.get(propName, String[].class);
        if (scopesSplit != null) {
            for (int i = 0; i < scopesSplit.length; i++) {
                String scope = scopesSplit[i];
                String[] scopeNameSplit = scope.split(ConnectorConstants.UNDERSCORE);
                if (!scopesSplit[i].startsWith(ConnectorConstants.FRIEND_STRING)
                        && !scopesSplit[i].startsWith(ConnectorConstants.USER_STRING)) {
                    append(extSb, scopeNameSplit);
                } else if (scopesSplit[i].startsWith(ConnectorConstants.USER_STRING)) {
                    append(userSb, scopeNameSplit);
                } else if (scopesSplit[i].startsWith(ConnectorConstants.FRIEND_STRING)) {
                    append(friendSb, scopeNameSplit);
                }
            }
        }

        addToConfigMap(configMap, ConnectorConstants.USER_PERMISSIONS, userSb.toString());
        addToConfigMap(configMap, ConnectorConstants.FRIEND_PERMISSIONS, friendSb.toString());
        addToConfigMap(configMap, ConnectorConstants.EXTNDED_PERMISSIONS, extSb.toString());

    }

    /**
     * Utility method to append and capitalize the first character of the string.
     * 
     * @param input
     *            input string
     * @param scopeNames
     *            array of permissions
     */
    private void append(StringBuilder input, String[] scopeNames) {
        if (input.toString().length() > 0) {
            input.append(ConnectorConstants.COMMA_SPACE);
        }
        for (int k = 0; k < scopeNames.length; k++) {
            if (k > 0) {
                input.append(ConnectorConstants.SPACE_STRING);
            }
            if (scopeNames[k].length() > 0) {
                input.append(scopeNames[k].substring(0, 1).toUpperCase()
                        + scopeNames[k].substring(1).toLowerCase());
            }
        }
    }

    /**
     * Utility method to prepare the Configurations map.
     * 
     * @param configMap
     *            Comma separated User Permissions
     * @param key
     *            Key of the map
     * @param value
     *            value of the map
     */
    private void addToConfigMap(Map<String, String> configMap, String key, String value) {
        if (value.length() > 0) {
            configMap.put(key, value);
        }
    }

    /**
     * This method is used to fetch the multi value property and to convert the value into comma
     * separated string.
     * 
     * @param properties
     *            Map of properties
     * @param propName
     *            Property Name
     * @return Comma separated string
     */
    private String convertArrayToString(ValueMap properties, String propName) {
        String propVals[] = properties.get(propName, String[].class);
        StringBuilder strValues = new StringBuilder();
        if (propVals != null && propVals.length > 0) {
            for (int i = 0; i < propVals.length; i++) {
                if (i > 0) {
                    strValues.append(ConnectorConstants.COMMA_SPACE);
                }
                strValues.append(propVals[i]);
            }
        }
        return strValues.toString();
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, String> getClientConfigurations(ValueMap properties) {
        Map<String, String> configMap = new LinkedHashMap<String, String>();

        // Client ID
        String clientId = properties.get(ConnectorConstants.OAUTH_CLIENT_ID, String.class);
        addToConfigMap(configMap, ConnectorConstants.APP_ID, clientId);
        
        // Secret Key
        String secretKey = properties.get(ConnectorConstants.OAUTH_CLIENT_SECRET, String.class);
        addToConfigMap(configMap, ConnectorConstants.APP_SECRET, secretKey);
        
        // Create User
        String oauthCreateUser = properties.get(ConnectorConstants.OAUTH_CREATE_USER, String.class);
        addToConfigMap(configMap, ConnectorConstants.CREATE_USERS, oauthCreateUser);
        
        // Encode User
        String maskedUsers = properties.get(ConnectorConstants.OAUTH_ENCODE_USER_ID, String.class);
        addToConfigMap(configMap, ConnectorConstants.MASK_USER_ID, maskedUsers);

        return configMap;
    }

}
