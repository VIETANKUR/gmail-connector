package com.accunity.connector.services;

import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.day.cq.wcm.api.Page;

/**
 * Interface designed to validate the configurations and to retrieve it.
 * 
 * @author Accunity Software
 *
 */
public interface ContentService {

    /**
     * This method is used to validate the configurations of the login connector.
     * 
     * @param currentPage
     *            reference to current page to obtain the properties. {@link Page}
     * @param resource
     *            reference to the resource object to obtain the child of the configurations.
     *            {@link Resource}
     * @param factoryPid
     *            Factory Process Id
     * @param configPropName
     *            property name whose value is fetched using current page reference
     * @return Error Message if invalid configurations
     */
    String validateConfigurations(Page currentPage, Resource resource, String factoryPid,
            String configPropName);

    /**
     * This method is used to retrieve the {@link Resource} properties of the given resource.
     * 
     * @param pageName
     *            name of the current page
     * @param resource
     *            parent resource. {@link Resource}
     * @param factoryPid
     *            Factory Process Id
     * @return Child Resource
     */
    Resource getConfigurationResource(String pageName, Resource resource, String factoryPid);

    /**
     * This method is used to retrieve all the configurations for the given connector.
     * 
     * @param properties
     *            List of properties
     * @return map of the configurations
     */
    Map<String, String> getConfigurations(ValueMap properties);

    /**
     * This method is used to retrieve all the client configurations for the given connector.
     * 
     * @param properties
     *            List of properties
     * @return map of the configurations
     */
    Map<String, String> getClientConfigurations(ValueMap properties);

}
