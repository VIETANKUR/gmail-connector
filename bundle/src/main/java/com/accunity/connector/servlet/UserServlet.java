package com.accunity.connector.servlet;

import java.io.IOException;
import java.util.Iterator;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFactory;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import com.accunity.connector.common.ConnectorConstants;
import com.day.cq.wcm.webservicesupport.ConfigurationManager;

@SlingServlet(paths = "/bin/user", methods = "POST", metatype = true)
public class UserServlet extends SlingAllMethodsServlet {

    @Reference
    ConfigurationManager cfgMgr;

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = -3773739779383554508L;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String resPath = request.getParameter("path");
        String[] userGroups = null;
        ResourceResolver resourceResolver = request.getResourceResolver();
        Resource pageResource = resourceResolver.getResource(resPath);

        Resource parentResource = pageResource.getChild(ConnectorConstants.JCR_CONTENT);

        Iterator<Resource> childItr = null;
        if (parentResource != null && parentResource.hasChildren()) {
            childItr = parentResource.listChildren();
        }
        while (childItr.hasNext()) {
            Resource childRes = childItr.next();
            ValueMap resProperties = childRes.getValueMap();
            if (resProperties.containsKey(ConnectorConstants.OAUTH_CREATE_USER_GROUPS)) {
                userGroups = resProperties.get(ConnectorConstants.OAUTH_CREATE_USER_GROUPS,
                        String[].class);
            }
        }

        Session session = resourceResolver.adaptTo(Session.class);
        UserManager userManager = resourceResolver.adaptTo(UserManager.class);
        User user = null;
        try {
            // check if user does exist, easy check with username, username is id in cq
            user = (User) userManager.getAuthorizable(email);
            if (user == null) {
                user = userManager.createUser(email, "password");
                ValueFactory valueFactory = session.getValueFactory();
                Value emailValue = valueFactory.createValue(email);
                Value nameValue = valueFactory.createValue(name);

                // User class just accepts Value Object
                user.setProperty("profile/" + "email", emailValue);
                user.setProperty("profile/" + "familyName", nameValue);
                user.setProperty("profile/" + "givenName", nameValue);
                for (String userGrp : userGroups) {
                    Group grp = (Group) userManager.getAuthorizable(userGrp);
                    grp.addMember(user);
                }
                session.save();
                session.logout();
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }
}
